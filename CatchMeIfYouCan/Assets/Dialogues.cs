﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Dialogues : MonoBehaviour
{
    public static Dialogues current;

    [SerializeField] public TMP_Text txtDialogues;
    [SerializeField] public GameObject dialoguesPanel;
   

    public bool isTalking = false;

    private void Awake()
    {
        current = this;
    }
}
