﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCRun : MonoBehaviour
{
    public static NPCRun current;
    public float speed;

    // Start is called before the first frame update
    void Awake()
    {
        current = this;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector2.left * speed * Time.deltaTime);
    }
}
