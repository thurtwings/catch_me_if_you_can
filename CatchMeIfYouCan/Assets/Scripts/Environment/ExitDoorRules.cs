﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitDoorRules : MonoBehaviour
{
    private Animator animator;

    private void Start()
    {
        animator = GetComponent<Animator>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        

        if (collision.gameObject.CompareTag("Player"))
        {
            if(GameManager.current.Enemies.Length != 0)
            {
                animator.SetBool("Open", false);
            }
            
            else
            {
                animator.SetBool("Open", true);
                StartCoroutine(GameManager.current.TimerWinPanel());
            }
        }
        else if (collision.gameObject.CompareTag("Enemy"))
        {
            animator.SetBool("Open", true);
            GameManager.current.enemyEscaped = true;
            Destroy(collision.gameObject);
        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        animator.SetBool("Open", false);
    }
}
