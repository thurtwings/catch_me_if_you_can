﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpItem : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Player") )
        {
            GameManager.current.pickedUpByPlayer.Add(this.gameObject);

        }
        else if (collision.gameObject.CompareTag("Enemy"))
        {
            GameManager.current.pickedUpByEnemies.Add(this.gameObject);
        }
        this.gameObject.SetActive(false);
    }
}
