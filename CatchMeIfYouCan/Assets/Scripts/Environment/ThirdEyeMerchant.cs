﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ThirdEyeMerchant : MonoBehaviour
{
    //    //Script permettant d'avoir des dialogues propres aux personnages

    //    //[SerializeField] Dialogues dialoguesOnOff;

    //    [SerializeField] private bool marchand = false;
    //    [SerializeField] private bool sleeping = false;
    //    public Animator animator;
    //    private bool isShoping;
    //    bool hpMaxUp = false;

    //    [TextArea(15, 20)]
    //    [SerializeField] public string localSpeech;
    //    [TextArea(15, 20)]
    //    [SerializeField] public string localSpeech2;
    //    [TextArea(15, 20)]
    //    [SerializeField] public string localSpeech3;

    //    //Shop
    //    [TextArea(15, 20)]
    //    [SerializeField] public string buySucces;
    //    [TextArea(15, 20)]
    //    [SerializeField] public string buyEchec;

    //    private void Start()
    //    {
    //        animator = animator.GetComponent<Animator>();
    //        if (sleeping)
    //        {
    //            animator.SetBool("Sleeping", true);
    //        }
    //        else if (!sleeping)
    //        {
    //            animator.SetBool("IdlePink", true);
    //            animator.SetBool("IdleRed", true);
    //        }
    //    }

    //    private void Update()
    //    {
    //        if (Dialogues.current.isTalking)
    //        {
    //            if (Input.GetMouseButtonDown(0))
    //            {
    //                SwapText();
    //            }
    //        }

    //    }

    //    void OnTriggerEnter(Collider other)
    //    {

    //        if (other.gameObject.tag == "Player")
    //        {
    //            HelpActive();
    //            if (Input.GetKeyDown(KeyCode.E))
    //            {
    //                StartDialogue();
    //                if (sleeping)
    //                {
    //                    animator.SetBool("Sleeping", true);
    //                }
    //            }
    //        }
    //    }
    //    private void OnTriggerStay(Collider other)
    //    {
    //        if (other.gameObject.tag == "Player")
    //        {
    //            if (!Dialogues.current.isTalking)
    //            {

    //                if (Input.GetKeyDown(KeyCode.E))
    //                {
    //                    StartDialogue();
    //                    if (sleeping)
    //                    {
    //                        animator.SetBool("Sleeping", true);
    //                    }
    //                }
    //            }
    //            if (marchand)
    //            {
    //                animator.SetBool("Idle", false);
    //                animator.SetTrigger("StartShop");

    //                if (isShoping)
    //                {
    //                    if (Input.GetKeyDown(KeyCode.Alpha1))
    //                    {
    //                        if (MoneyCount.current.nbMoney >= 100)
    //                        {
    //                            animator.SetTrigger("Succes");
    //                            Dialogues.current.txtDialogues.text = buySucces;
    //                            MoneyCount.current.nbMoney -= 100;
    //                            PlayerHealth.current.playerHealth = HealthBarScript.current.maxHealth;
    //                            HealthBarScript.current.health = HealthBarScript.current.maxHealth;
    //                            Debug.Log("Yes");
    //                            StopShop();
    //                        }
    //                        else
    //                        {
    //                            animator.SetTrigger("NotEnoughMoney");
    //                            Dialogues.current.txtDialogues.text = buyEchec;
    //                            Debug.Log("No Money");
    //                            StopShop();
    //                        }

    //                    }
    //                    else if (Input.GetKeyDown(KeyCode.Alpha2))
    //                    {
    //                        if (MoneyCount.current.nbMoney >= 200)
    //                        {
    //                            animator.SetTrigger("Succes");
    //                            Dialogues.current.txtDialogues.text = buySucces;
    //                            MoneyCount.current.nbMoney -= 200;
    //                            if (!hpMaxUp)
    //                            {
    //                                HealthBarScript.current.maxHealth = 20000f;
    //                                PlayerHealth.current.maxHealth = 20000f;
    //                                hpMaxUp = true;
    //                            }
    //                            else
    //                            {
    //                                Debug.Log("Already bought");
    //                                animator.SetTrigger("NotEnoughMoney");
    //                            }
    //                            Debug.Log("Yes");
    //                            StopShop();
    //                            //Dialogues.current.txtDialogues.text = buyEchec;
    //                            //Debug.Log("No Money");
    //                            //StopShop();
    //                        }
    //                        else
    //                        {
    //                            animator.SetTrigger("NotEnoughMoney");
    //                            Dialogues.current.txtDialogues.text = buyEchec;
    //                            Debug.Log("No Money");
    //                            StopShop();
    //                        }
    //                    }
    //                    else if (Input.GetKeyDown(KeyCode.Alpha3))
    //                    {
    //                        if (MoneyCount.current.nbMoney >= 9999)
    //                        {
    //                            //animator.SetTrigger("Succes");
    //                            //Dialogues.current.txtDialogues.text = buySucces;
    //                            //MoneyCount.current.nbMoney -= 50;
    //                            //Debug.Log("Yes");
    //                            //StopShop();
    //                            animator.SetTrigger("NotEnoughMoney");
    //                            Dialogues.current.txtDialogues.text = buyEchec;
    //                            Debug.Log("No Money");
    //                            StopShop();
    //                        }
    //                        else
    //                        {
    //                            animator.SetTrigger("NotEnoughMoney");
    //                            Dialogues.current.txtDialogues.text = buyEchec;
    //                            Debug.Log("No Money");
    //                            StopShop();
    //                        }
    //                    }
    //                }
    //            }


    //        }
    //    }
    //    private void OnTriggerExit(Collider other)
    //    {
    //        if (other.gameObject.tag == "Player")
    //        {
    //            animator.SetBool("Idle", true);
    //            StopText();
    //            if (marchand)
    //            {
    //                StopShop();
    //            }
    //        }
    //    }

    //    public void HelpActive()
    //    {
    //        Dialogues.current.help.SetActive(true);
    //    }

    //    public void StartDialogue()
    //    {
    //        SoundsManager.current.PlayThisSound(Random.Range(2, 5));
    //        Dialogues.current.help.SetActive(false);
    //        Dialogues.current.txtDialogues.text = localSpeech;
    //        Dialogues.current.isTalking = true;
    //        Dialogues.current.dialoguesPanel.SetActive(true);
    //    }

    //    public void StartShop()
    //    {
    //        isShoping = true;
    //        Dialogues.current.shopPanel.SetActive(true);
    //    }

    //    public void StopShop()
    //    {
    //        Dialogues.current.shopPanel.SetActive(false);
    //        isShoping = false;
    //    }

    //    public void SwapText()
    //    {
    //        if (Dialogues.current.txtDialogues.text == localSpeech)
    //        {
    //            Dialogues.current.txtDialogues.text = localSpeech2;
    //            if (sleeping)
    //            {
    //                animator.SetBool("Sleeping", false);
    //            }
    //            SoundsManager.current.PlayThisSound(Random.Range(2, 5));
    //        }
    //        else if (Dialogues.current.txtDialogues.text == localSpeech2)
    //        {
    //            Dialogues.current.txtDialogues.text = localSpeech3;
    //            if (sleeping)
    //            {
    //                animator.SetBool("Sleeping", true);
    //            }
    //            SoundsManager.current.PlayThisSound(Random.Range(2, 5));
    //        }
    //        else if (Dialogues.current.txtDialogues.text == localSpeech3)
    //        {
    //            if (marchand && !isShoping)
    //            {

    //                StartShop();
    //            }
    //            else
    //            {
    //                StopText();
    //            }
    //        }
    //        else if (Dialogues.current.txtDialogues.text == buySucces || Dialogues.current.txtDialogues.text == buyEchec)
    //        {
    //            StopText();
    //        }

    //    }

    //    public void StopText()
    //    {
    //        Dialogues.current.help.SetActive(false);
    //        Dialogues.current.dialoguesPanel.SetActive(false);
    //        Dialogues.current.isTalking = false;
    //    }
    //    void MerchantGiveTheEye()
    //    {

    //    }




}
