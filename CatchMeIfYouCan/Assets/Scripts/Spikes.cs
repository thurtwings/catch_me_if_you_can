﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spikes : MonoBehaviour
{
    private EnemyHealth enemyHealth;
    public GameObject winPanel;
    public int damagesOnAI;
    public int damagesOnPlayer;
    
    bool tookDamages = false;
    bool killedByTrap;
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Enemy"))
        {
            killedByTrap = col.gameObject.GetComponent<EnemyHealth>().killedByTrap = true;
            if (enemyHealth == null)
            {
                enemyHealth = col.gameObject.GetComponent<EnemyHealth>();
                enemyHealth.TakeDamages(damagesOnAI);
            }
            else if (enemyHealth != null)
            {
                enemyHealth = col.gameObject.GetComponent<EnemyHealth>();
                enemyHealth.TakeDamages(damagesOnAI);
            }


        }
        if (col.gameObject.CompareTag("Player"))
        {
            if (!tookDamages)
            {
                PlayerHealth.current.health -= damagesOnPlayer;
                
                tookDamages = true;
            }


            if (PlayerHealth.current.health == 0)
            {
                PlayerMoves.current.isGulty = false;


            }
            tookDamages = false;
        }

    }
}
