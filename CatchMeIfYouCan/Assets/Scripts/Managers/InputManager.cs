﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public static InputManager current;
    public Animator animator_;
    public bool isCrouching = false;
    public GameObject thirtdEye;
    public GameObject invisibleWorld;
    public GameObject visibleWorld;
    public bool hasAttacked = false;
    private void Awake()
    {
        current = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        animator_ = animator_.GetComponent<Animator>();
        invisibleWorld.SetActive(false); 
        visibleWorld.SetActive(true); 
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            GameManager.current.Pause();
        }
        if (Input.GetKey(KeyCode.S))
        {
            PlayerMoves.current.isCrouched = true;
            //animator_.SetBool("Idle", false);
            //animator_.SetBool("Crouch", true);


        }
        else
        {
            PlayerMoves.current.isCrouched = false;
            //animator.SetBool("Crouch", false) ;
        }
        if (Input.GetMouseButtonDown(1))
        {
            StartCoroutine(DoubleView());
        }
        if (Input.GetMouseButtonDown(0) && !PlayerHealth.current.isDead)
        {
           
            
            animator_.Play("PlayerAttack1");
            //PlayerDamages.current.audio.clip = PlayerDamages.current.audioClips[0];
            //PlayerDamages.current.audio.Play();
            PlayerMoves.current.moveSpeed = 0;
            
            
            
        }
        else
        {
            PlayerMoves.current.moveSpeed = 250;
        }
    }

    IEnumerator DoubleView()
    {
        invisibleWorld.SetActive(true);
        visibleWorld.SetActive(false);
        thirtdEye.SetActive(true);
        
        yield return new WaitForSeconds(5f);
        thirtdEye.SetActive(false);
        visibleWorld.SetActive(true);

        invisibleWorld.SetActive(false);

    }
}
