﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameManager : MonoBehaviour
{
    public static GameManager current;
    public GameObject player;
    public Transform levelStartPoint;
    public GameObject pausePanel;
    public GameObject winPanel;
    public GameObject ItemsPickedUpByPlayerPanel;
    public GameObject ItemsPickedUpByEnemiesText;
    public GameObject gameOverPanel;
    public GameObject enemyEscapedPanel;
    public GameObject IAs;
    public TMP_Text allEnemiesDeadNotice;
    public TMP_Text allItemsPickedUpText;
    public PickUpItem[] itemsToPickUp;
    public bool isPaused;
    public bool allItemsPickedUp;
    public EnemyHealth[] Enemies;
    public List<GameObject> deadBodiesConfirmed = new List<GameObject>();
    public List<GameObject> pickedUpByPlayer = new List<GameObject>();
    public List<GameObject> pickedUpByEnemies = new List<GameObject>();
    public bool enemyEscaped = false;
    public bool gameOver;
    private void Awake()
    {
        current = this;
        Time.timeScale = 1f;
    }
    // Start is called before the first frame update
    void Start()
    {
        allItemsPickedUp = false;
        gameOver = false;
        pausePanel.SetActive(false);
        winPanel.SetActive(false);
        gameOverPanel.SetActive(false);
        ItemsPickedUpByPlayerPanel.SetActive(false);
        ItemsPickedUpByEnemiesText.SetActive(false);
        enemyEscapedPanel.SetActive(false);
        isPaused = false;
        Enemies = FindObjectsOfType<EnemyHealth>();
        itemsToPickUp = FindObjectsOfType<PickUpItem>();
    }

    // Update is called once per frame
    void Update()
    {
        allItemsPickedUp = pickedUpByEnemies.Count + pickedUpByPlayer.Count == itemsToPickUp.Length;
        if (allItemsPickedUp)
        {
            allItemsPickedUpText.gameObject.SetActive(true);
            if(allItemsPickedUp && deadBodiesConfirmed.Count == Enemies.Length)
            {
                allEnemiesDeadNotice.gameObject.SetActive(false);
                allItemsPickedUpText.gameObject.SetActive(false);
                StartCoroutine(TimerWinPanel());
            }
        }
        
        if (deadBodiesConfirmed.Count == Enemies.Length)
        {
            Destroy(IAs, 1f);
            allEnemiesDeadNotice.gameObject.SetActive(true);
            
        }
        else
        {
            allEnemiesDeadNotice.gameObject.SetActive(false);
        }
        if(pickedUpByPlayer.Count == itemsToPickUp.Length  )
        {
            
            StartCoroutine(TimerAllItemsPickedByPlayer());
        }
        else if(pickedUpByEnemies.Count == itemsToPickUp.Length  )
        {
            
            ItemsPickedUpByEnemiesText.SetActive(true);
        }
        if (enemyEscaped && !gameOver)
        {
            player.GetComponent<PlayerMoves>().enabled = false;
            player.GetComponent<Animator>().SetBool("Idle", false);
            player.GetComponent<Animator>().SetBool("Dead", true);
            ItemsPickedUpByEnemiesText.SetActive(false);
            enemyEscapedPanel.SetActive(true);
            Time.timeScale = 0f;
        }
        
    }

    public IEnumerator TimerWinPanel()
    {
        yield return new WaitForSeconds(2f);
        winPanel.SetActive(true);
        Time.timeScale = 0f;
        
    }
    IEnumerator TimerAllItemsPickedByPlayer()
    {
        yield return new WaitForSeconds(.1f);
        ItemsPickedUpByPlayerPanel.SetActive(true);
        Time.timeScale = 0f;
        
    }
    public void Retry() 
    {
        SceneManager.LoadScene(1);
        
    }
    public void GameOver()
    {
        if (!gameOver) 
        {
            gameOver = true;
            player.GetComponent<PlayerMoves>().enabled = false;
            player.GetComponent<Animator>().SetBool("Idle", false);
            player.GetComponent<Animator>().SetBool("Dead", true);
            gameOverPanel.SetActive(true);
        }
       

    }
    public void Pause()
    {
        isPaused = !isPaused;
        if (isPaused)
        {
            pausePanel.SetActive(true);
            Time.timeScale = 0;
        }
        else
        {
            pausePanel.SetActive(false);
            Time.timeScale = 1;
        }
        
        
    }
    public void OnApplicationQuit()
    {
        Application.Quit();
    }
    
}
