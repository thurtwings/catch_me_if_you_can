﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnakesTrigger : MonoBehaviour
{
    public int damages;
    private EnemyHealth enemyHealth;
    public bool killedByTrap = false;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            PlayerHealth.current.health -= damages;
        }
        else if (collision.gameObject.CompareTag("Enemy"))
        {

            enemyHealth = collision.gameObject.GetComponent<EnemyHealth>();
            enemyHealth.TakeDamages(damages);
            if(enemyHealth.hitPoints <= 0)
            {
                killedByTrap = collision.gameObject.GetComponent<EnemyHealth>().killedByTrap = true;
            }
        }
    }
}
