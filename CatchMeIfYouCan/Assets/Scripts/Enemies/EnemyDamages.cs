﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDamages : MonoBehaviour
{
    public PlayerHealth playerHealth;
    public int attackForce;
    AIScript ai;
    bool tookDamages = false;
    public AudioSource audioHurt;
    private void Start()
    {
        ai = GetComponent<AIScript>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.CompareTag("Player"))
        {
            if (!tookDamages)
            {
                PlayerHealth.current.health -= attackForce;
                audioHurt.Play();
                Debug.Log("+1 sound");
                tookDamages = true;
            }
            
            
            if (PlayerHealth.current.health == 0)
            {
                PlayerMoves.current.isGulty = false;
                

            }
            tookDamages = false;
        }
    }
}
