﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnakesScript : MonoBehaviour
{
    public bool canSeeTarget;
    public bool canAttackTarget;
    public Vector2 snakeFoV;
    public Vector2 snakeAttackRange;
    public Vector3 offsetFoV;
    public Vector3 offsetAttack;
    public LayerMask targetLayer;
    Animator animator;
    public GameObject biteTrigger;
    public bool facingRight = false;
    private void Start()
    {
        animator = GetComponent<Animator>();
        
    }
    private void FixedUpdate()
    {
        if(canSeeTarget = Physics2D.OverlapBox(transform.position + offsetFoV, snakeFoV, 0, targetLayer))
        {
            animator.SetBool("CanAttack", true);

        }
        else
        {
            animator.SetBool("CanAttack", false);
        }
        if(canAttackTarget = Physics2D.OverlapBox(transform.position + offsetAttack, snakeAttackRange, 0, targetLayer))
        {
            animator.SetBool("Attack", true);
        }
        else
        {
            animator.SetBool("Attack", false);
        }
        
    }
    void AttackTrue()
    {
        biteTrigger.SetActive(true);
    }
    void AttackFalse()
    {
        biteTrigger.SetActive(false);
    }
    private void OnDrawGizmosSelected()
    {
        
        Gizmos.color = Color.black; // box to see the field of view
        Gizmos.DrawWireCube((transform.position + offsetFoV), snakeFoV);
        Gizmos.color = Color.red; // box to see the field of view
        Gizmos.DrawWireCube((transform.position + offsetAttack), snakeAttackRange);
    }
}
