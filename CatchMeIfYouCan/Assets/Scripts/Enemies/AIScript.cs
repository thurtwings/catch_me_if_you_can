﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.SocialPlatforms;

public class AIScript : MonoBehaviour
{
    #region Variables
    // Variables de base
    [Header("Basics ")]
    public float moveSpeed;
    private Rigidbody2D rb;
    Animator animator;

    // Variables pour les verif' environement 
    [Header("For Environment Verifications")]
    float moveDirection = 1;
    public bool facingRight = true;
    [SerializeField] Transform runCheckPoint;
    [SerializeField] Transform wallCheckPoint;
    [SerializeField] float radius;
    [SerializeField] LayerMask groundLayer;
    public bool checkingGround;
    public bool checkingWall;
        
    // Variables pour la fonction de saut
    [Header("For Jumping")]
    [SerializeField] float jumpForce;
    [SerializeField] Transform groundCheckPoint;
    [SerializeField] Transform player;
    [SerializeField] Vector2 boxSize;
    public bool isGrounded;

    //Variables pour détecter les actions du joueur
    [Header("For Seeing the Player")]
    
    [SerializeField] LayerMask playerLayer;
    public bool canSeePlayer;
    public bool sawPlayerAtking = false;
    [SerializeField] Vector2 fieldOfView;


    // Variables pour détecter les morts
    // Variable pour suivre le player
    [Header("Recognizing Dead IA & Suspecting the player if he's in the range")]
    public LayerMask deadBody;
    public bool suspectingPlayer;
    public bool seeBody;
    public bool investigatingBody = false;
    public Vector2 followDistance;
    public GameObject deadBuddy;
    public GameObject statMark;
    public Sprite questionMark;
    public Sprite exclamationMark;
     

    //faire une liste pour les corps

    [Header("For Attacking")]
    public GameObject swordTrigger;
    //public bool attackPlayer;

    [Header("For TimerJump")]
    public float jumpCooldown = 0.5f;
    public float lastJump =0f;

    [Header("To Make Sure What Killed the IA")]
    
    public bool deadBuddyKilledByTrap;


    // Variables pour détecter les plateformes du niveau
    // Variables pour détecter les objets du niveau

    #endregion
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        deadBuddy = null;
        suspectingPlayer = false;
        seeBody = false;
        sawPlayerAtking = false;
        statMark.gameObject.GetComponent<SpriteRenderer>().sprite = null;
    }

    //conditions pour 
    //voir le player 
    //voir les morts
    // l'attaquer 
    // continuer le niveau
    private void Update()
    {
        if (isGrounded && !seeBody )
        { 
            // a changer pour mettre une réaction rdm pour le faire courir vers les autres IA et les prévenir
            if (canSeePlayer && PlayerMoves.current.isAttacking )
            {

                sawPlayerAtking = true;
                if (sawPlayerAtking)
                {
                    Attack();
                    statMark.gameObject.GetComponent<SpriteRenderer>().sprite = exclamationMark;

                }
            }
            else if (sawPlayerAtking) 
            {
                Attack();
                statMark.gameObject.GetComponent<SpriteRenderer>().sprite = exclamationMark;
            }
            else if (PlayerMoves.current.isGulty)
            {
                if (sawPlayerAtking)
                {
                    Attack();
                    statMark.gameObject.GetComponent<SpriteRenderer>().sprite = exclamationMark;
                }

            }
            

            Running();
        }
        if (canSeePlayer && seeBody && deadBuddy != null && !deadBuddyKilledByTrap)
        {
            suspectingPlayer = true;
            Debug.Log("suspect Player");
            Attack();
        }
        if(rb.velocity.x < 0.1f || rb.velocity.x > 0.1f)
        {
            animator.SetBool("IsRunning", true);
            animator.SetBool("Idle", false);

        }
        else if(rb.velocity.x == 0f || rb.velocity.x == 0f)
        {
            animator.SetBool("IsRunning", false);
            animator.SetBool("Idle", true);

        }
        if (canSeePlayer && deadBuddy != null && suspectingPlayer && !deadBuddyKilledByTrap)
        {

            if (Mathf.Abs(transform.position.x - player.position.x) > 2 )
            {
                moveSpeed = 4;
                transform.position = Vector2.MoveTowards(transform.position, player.position , moveSpeed * Time.deltaTime);
                animator.SetBool("IsRunning", true);
                animator.SetBool("Idle", false);
                FlipTowardsPlayer();
                
            }
            else if (PlayerMoves.current.isAttacking && sawPlayerAtking)
            {
                suspectingPlayer = false;
                investigatingBody = false;
                Attack();

            }
            else
            {
                animator.SetBool("Idle", true);
                animator.SetBool("IsRunning", false);
                moveSpeed = 0;
            }
            

        }



    }
    // Actions relatives à la physique à mettre dans fixedupdate
    // Créer des zones de détection avec overlap sur les différents points de vérifications
    
    void FixedUpdate()
    {
        checkingGround = Physics2D.OverlapCircle(runCheckPoint.position, radius, groundLayer);
        checkingWall = Physics2D.OverlapCircle(wallCheckPoint.position, radius, groundLayer);
        isGrounded = Physics2D.OverlapBox(groundCheckPoint.position, boxSize, 0, groundLayer);
        canSeePlayer = Physics2D.OverlapBox(transform.position, fieldOfView, 0, playerLayer);

        if(seeBody = Physics2D.OverlapBox(transform.position, fieldOfView, 0, deadBody))
        {
            statMark.GetComponent<SpriteRenderer>().sprite = questionMark;
            
            Investigate();
            
            deadBuddy = Physics2D.OverlapBox(transform.position, fieldOfView, 0, deadBody).gameObject;
            deadBuddyKilledByTrap = deadBuddy.GetComponent<EnemyHealth>().killedByTrap;
        }
    }
    // la fonction de run doit vérifier si on est au sol, si on est face à un mur, et le sens dans lequel l'ia est
    // faire bouger l'ia
    public void Running()
    {
        if (!checkingGround || checkingWall)
        {
            if (checkingWall && facingRight)
            {
                Flip();
            }
            else if (!facingRight && checkingWall)
            {
                Flip();
            }

            if (facingRight && !checkingGround)
            {
                Jump();
            }
            else if (!facingRight && !checkingGround)
            {
                Jump();
            }

        }

            animator.SetBool("IsRunning", true);

            rb.velocity = new Vector2(moveSpeed * moveDirection, rb.velocity.y);
        
    }
     // la fonction de saut doit :
        //calculer la distance entre l'ia et la destination //////////TODOOOOOOOOOOOO///////////  
        // faire sauter l'ia avec une impulsion vers la destination
    void Jump()
    {
         if ((lastJump + jumpCooldown < Time.time) && isGrounded && !checkingWall)
         {
             animator.SetTrigger("Jump");
             rb.AddForce(new Vector2(0, jumpForce), ForceMode2D.Impulse);
             lastJump = Time.time;
         }
        
    }
    // se tourner vers le player
    void FlipTowardsPlayer()
    {
        float playerPosition = player.position.x - transform.position.x;
        if(playerPosition < 0 && facingRight)
        {
            Flip();
        }
        else if(playerPosition > 0 && !facingRight)
        {
            Flip();
            
        }

    }

    //La fonction attack doit se lancer quand elle est a bonne distance du player pour ne pas attaquer dans le vide 
    void Attack()
    {
        
        float playerPositionX = player.position.x - transform.position.x;
        float playerPositionY = player.position.y - transform.position.y;
        //moveSpeed = 4;
        if ( sawPlayerAtking && canSeePlayer)
        {
                if (playerPositionX < .5f || playerPositionY < .5f )
                {

                    rb.constraints = RigidbodyConstraints2D.FreezeAll;
                    animator.SetTrigger("Attack");
                    animator.SetBool("IsRunning", false);
                    
                }
            
        }
        else if (canSeePlayer && deadBuddy != null && suspectingPlayer)
        {
            
            if (playerPositionX > .5f || playerPositionY > .5f )
            {

                //rb.constraints = RigidbodyConstraints2D.FreezeAll;

                animator.SetTrigger("Attack");
                animator.SetBool("IsRunning", false);
            }

        }
        else if (!canSeePlayer && deadBuddy != null && !suspectingPlayer)
        {
            moveSpeed = 4;
            Running();
        }

        if (playerPositionX > .5f || playerPositionY > .5f || playerPositionX < -.5f || playerPositionY < -.5f)
        {
            rb.constraints = RigidbodyConstraints2D.None;
            rb.constraints = RigidbodyConstraints2D.FreezeRotation;
            Running();
            
        }
        FlipTowardsPlayer();
    }
    // detecter si elle doit se tourner
    void Flip()
    {
        moveDirection *= -1;
        facingRight = !facingRight;
        transform.Rotate(0, 180, 0);
    }

    //La fonction Investigate doit:
    //se diriger près du corps trouvé 
    //calculer si le player est a moins d'une distance donnée
    // en fonction de la distance, soupçonner le player
    //déterminer si l'ia est tuée par player ou piege
    void Investigate()
    {
        if (!suspectingPlayer && !investigatingBody)
        {
            investigatingBody = true;
            StartCoroutine(TimerToLookOutForKiller());

        }
        else if (suspectingPlayer && !canSeePlayer && !sawPlayerAtking)
        {
            FlipTowardsPlayer();
            
        }

        else if (suspectingPlayer && canSeePlayer && sawPlayerAtking)
        {

            Attack();
        }


    }

    //permet de bloquer le joueur quelques temps en restant pres de lui
    //si l'ia est tuée a ce moment, les autres chercheront le player pour l'attaquer

    //Coroutine pour "enqueter"
    IEnumerator TimerToLookOutForKiller()
    {
        statMark.gameObject.GetComponent<SpriteRenderer>().sprite = questionMark;
        moveSpeed = 0;
        animator.SetBool("IsRunning", false);
        animator.SetBool("Idle", true);
        
        yield return new WaitForSeconds(1.5f);
        
        Flip();
        
        yield return new WaitForSeconds(1.5f);
        Flip();
       
        yield return new WaitForSeconds(1.5f);
        if (deadBuddyKilledByTrap)
        {
            moveSpeed = 4;
            Running();
        }
        deadBuddy.SetActive(false);

        if (canSeePlayer && seeBody && !deadBuddyKilledByTrap)
        {
            suspectingPlayer = true;
        }
        else if(!canSeePlayer && deadBuddy != null && !suspectingPlayer )
        {
            suspectingPlayer = false;
            moveSpeed = 4;
            Running();
        }
        else
        {
            suspectingPlayer = false;
            moveSpeed = 4;
            Running();
            
        }
        
        
    }
    void SwordOut()
    {
        swordTrigger.gameObject.SetActive(true);
        if (!facingRight)
        {
            swordTrigger.transform.localPosition = new Vector3(0.45f, 0, 0);
        }
        else
        {
            swordTrigger.transform.localPosition = new Vector3(0.45f, 0, 0);
        }
    }
    void SwordIn()
    {
        swordTrigger.gameObject.SetActive(false);
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red; // circle to see the runcheckpoint
        Gizmos.DrawWireSphere(runCheckPoint.position, radius);

        Gizmos.color = Color.yellow; // circle to see the wallcheckpoint
        Gizmos.DrawWireSphere(wallCheckPoint.position, radius);

        Gizmos.color = Color.blue; // box to see the groundcheckpoint
        Gizmos.DrawCube(groundCheckPoint.position, boxSize);

        Gizmos.color = Color.cyan; // box to see the field of view
        Gizmos.DrawWireCube(transform.position, fieldOfView);

        Gizmos.color = Color.black; // box to see the field of view
        Gizmos.DrawWireCube(transform.position, fieldOfView);
    }
}
