﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    // Tuto link https://www.youtube.com/watch?v=v1UGTTeQzbo

    // Distorted Pixel Studios

    public int hitPoints;
    public int maxHitPoints = 5;
    public EnemyHealthbar healthbar;
    Animator animator;
    public bool isDead;
    public bool killedByTrap;
    void Start()
    {
        hitPoints = maxHitPoints;
        isDead = false;
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        healthbar.SetHealth(hitPoints, maxHitPoints);
    }

    public void TakeDamages(int damages)
    {
        hitPoints -= damages;
        //play hurt anim

        if (hitPoints <= 0)
        {
            animator.SetBool("IsDead", true);
            animator.SetBool("Idle", false);
            this.gameObject.layer = 11;
            this.gameObject.tag = "DeadBody";
            this.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePositionX;
            this.GetComponent<AIScript>().statMark.SetActive(false);
            this.GetComponent<AIScript>().enabled = false;
            this.transform.Find("SwordTrigger").gameObject.SetActive(false);
            
            isDead = true;
            GameManager.current.deadBodiesConfirmed.Add(this.gameObject);
        }
        //Destroy(this.gameObject, 10f);

        PlayerMoves.current.isGulty = true;
        
    }
}
