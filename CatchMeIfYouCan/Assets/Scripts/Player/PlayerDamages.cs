﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public class PlayerDamages : MonoBehaviour
{
    public static PlayerDamages current;
    private EnemyHealth enemyHealth;
    public GameObject winPanel;
    public int damages;
    public AudioSource audio;
    public AudioClip[] audioClips;

    private void Awake()
    {
        current = this;
    }
    private void Start()
    {
        enemyHealth = null;
        if(enemyHealth == null)
        {
            audio.clip = audioClips[0];
            audio.Play();
        }
    }
    
    private void OnTriggerEnter2D(Collider2D col) 
    {
        if (col.gameObject.CompareTag("Enemy"))
        {
            
            StartCoroutine(TimerAttackingState());

            if (enemyHealth == null)
            {
                audio.clip = audioClips[1];
                audio.Play();
                enemyHealth = col.gameObject.GetComponent<EnemyHealth>();
                enemyHealth.TakeDamages(damages);
            }
            else if (enemyHealth != null)
            {
                audio.clip = audioClips[1];
                audio.Play();
                enemyHealth = col.gameObject.GetComponent<EnemyHealth>();
                enemyHealth.TakeDamages(damages);
                
            }
            
        }
        
    }

    IEnumerator TimerAttackingState()
    {
        PlayerMoves.current.isAttacking = true;
        yield return new WaitForSeconds(2f);
        PlayerMoves.current.isAttacking = false;
    }
}
