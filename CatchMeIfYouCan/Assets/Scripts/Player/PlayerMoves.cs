﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMoves : MonoBehaviour
{
    //Singleton
    public static PlayerMoves current;

    //Variables de base : speed, jump, rb, anim etc...
    [Header("Basics")]
    public float moveSpeed;
    public float jumpForce;
    public Rigidbody2D rb;
    private Vector3 velocity = Vector3.zero;
    Animator animator;
    public Transform swordTrigger;
    
    //Variable de verification
    [Header("Verifs")]
    public bool isJumping;
    public bool canDoubleJump = false;
    public bool isOnTheGround;
    public bool isCrouched;
    public bool isMoving;
    public bool isGulty;
    public bool isAttacking = false;
    public Transform gc;
    public SpriteRenderer spriteRenderer;

   
    //piquer la fonction gravité que rémi a fait pour le proto
    [Header("Gravity")]
    public float gravity = -9.8f;
    public float gravityScale = 1f;
    public bool gravityEnabled = true;

    float horizontalMovement;
    private void Awake()
    {
        current = this;
    }
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        if (isOnTheGround)
        {
           canDoubleJump = false;
        }
        if (Input.GetButtonDown("Jump") && isOnTheGround && !isJumping && !canDoubleJump)
        {
            animator.Play("PlayerJump");
            isJumping = true;
            
            
            
        }
        if (Input.GetButtonDown("Jump") && canDoubleJump)
        {
            DoubleJump();
        }

        if (isOnTheGround && isCrouched)
        {
            moveSpeed /= moveSpeed ;
            animator.SetBool("Idle", false);
            animator.SetBool("Crouch", true);
        }
        else
        {
            moveSpeed = 250;
            animator.SetBool("Idle", true);
            animator.SetBool("Crouch", false);
        }
        
        Flip(rb.velocity.x);
        
    }

    // Actions relatives à la physique à mettre dans fixedupdate 
    void FixedUpdate()
    {
        horizontalMovement = Input.GetAxis("Horizontal") * moveSpeed * Time.deltaTime;
        
        MovePlayer(horizontalMovement);

        //Gravity piqué du protype et modif' pour la 2D
        if (gravityEnabled)
        {
            Physics2D.gravity = new Vector2(0f, -20f);
            rb.AddForce(Vector2.up * gravity * gravityScale * Time.deltaTime);
        }
    }


    private void OnTriggerStay2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Ground") && !InputManager.current.isCrouching)
        {
            isOnTheGround = true;
            isJumping = false;
            animator.Play("Idle");
            
        }
        
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        isOnTheGround = false;
    }


    public void MovePlayer(float pHorizontalMovement)
    {
        Vector3 lTargetVelocity = new Vector2(pHorizontalMovement, rb.velocity.y);
        rb.velocity = Vector3.SmoothDamp(rb.velocity, lTargetVelocity, ref velocity, .05f);
        
        if (isJumping)
        {
            
            rb.AddForce(new Vector2(0f, jumpForce));
            animator.Play("PlayerJump");
            canDoubleJump = true;
            isJumping = false;

        }
        
    }
    void DoubleJump()
    {
        rb.AddForce(new Vector2(0f, jumpForce));
        animator.SetTrigger("DoubleJump");
        isJumping = false;
        canDoubleJump = false;
    }
    void Flip(float pVelocity)
    {
        if(pVelocity > 0.15f && !isJumping)
        {
            isMoving = true;
            spriteRenderer.flipX = false;
            animator.SetBool("Idle", false);
            animator.SetBool("Move", true);
            if (isJumping)
            {
                animator.Play("PlayerJump");
            }
            if (isCrouched && isMoving)
            {
                animator.Play("PlayerStand");
            }
        }
        else if(pVelocity < -0.15f && !isJumping)
        {
            isMoving = true;
            spriteRenderer.flipX = true;
            animator.SetBool("Idle", false);
            animator.SetBool("Move", true);
            if (isJumping)
            {
                animator.Play("PlayerJump") ;
            }
            if (isCrouched && isMoving)
            {
                animator.Play("PlayerStand");
            }

        }
        else 
        {
            isMoving = false;
            animator.SetBool("Idle", true);
            animator.SetBool("Move", false);
        }
       
    }

    void SwordOut()
    {
        swordTrigger.gameObject.SetActive(true);
        
        if (spriteRenderer.flipX)
        {
            swordTrigger.transform.localPosition =new Vector3(-0.45f, 0, 0);
        }
        else
        {
            swordTrigger.transform.localPosition = new Vector3(0.45f, 0, 0);
        }
    }
    void SwordIn()
    {
        swordTrigger.gameObject.SetActive(false);
        isAttacking = false;
    }


}
