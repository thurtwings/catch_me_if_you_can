﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{
    public static PlayerHealth current;

    public int health;
    public int numberOfHearts;

    public bool isDead = false;
    public Image[] hearts;
    public Sprite fullHeart;
    public Sprite emptyHeart;



    private void Awake()
    {
        current = this;
    }
    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < hearts.Length; i++)
        {
            if(i< health)
            {
                hearts[i].sprite = fullHeart;
            }
            else
            {
                hearts[i].sprite = emptyHeart;
            }
            if(i < numberOfHearts)
            {
                hearts[i].enabled = true;
            }
            else
            {
                hearts[i].enabled = false;
            }
        }
        if(health <= 0 && !isDead)
        {
            isDead = true;
            GameManager.current.GameOver();
        }
    }

    void IsDead()
    {
        isDead = true;
        
    }
}
