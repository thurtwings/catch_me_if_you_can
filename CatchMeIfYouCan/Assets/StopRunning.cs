﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopRunning : MonoBehaviour
{
    public static StopRunning current;
    public Animator animatorNPC;
    public Animator[] animatorsIA;
    public IAMoveIntro[] speedIA;


    private void Awake()
    {
        current = this;
    }
    private void Start()
    {
        animatorNPC = animatorNPC.gameObject.GetComponent<Animator>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("NPC"))
        {
            Debug.Log("NPC");
            animatorNPC.SetBool("Idle", true);
        }
        else if (collision.CompareTag("IA"))
        {
            foreach (Animator anim in animatorsIA)
            {
                anim.SetBool("Idle", true);
            }
            foreach(IAMoveIntro ia in speedIA)
            {
                ia.speed = 0f;
            }
        }
    }
}
