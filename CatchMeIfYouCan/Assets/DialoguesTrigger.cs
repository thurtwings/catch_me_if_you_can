﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialoguesTrigger : MonoBehaviour
{
    [TextArea(5, 20)]
    [SerializeField] public string localSpeech;
    [TextArea(5, 20)]
    [SerializeField] public string localSpeech2;
    [TextArea(5, 20)]
    [SerializeField] public string localSpeech3;
    [TextArea(5, 20)]
    [SerializeField] public string localSpeech4;
    [TextArea(5, 20)]
    [SerializeField] public string localSpeech5;
    [TextArea(5, 20)]
    [SerializeField] public string localSpeech6;
    [TextArea(5, 20)]
    [SerializeField] public string localSpeech7;
    [TextArea(5, 20)]
    [SerializeField] public string localSpeech8;
    [TextArea(5, 20)]
    [SerializeField] public string localSpeech9;
    [TextArea(5, 20)]
    [SerializeField] public string localSpeech10;
    public GameObject[] ias;
    public SpriteRenderer npc;
    public Animator camAnim;
    public GameObject spell;
    private void Start()
    {
        spell.SetActive(false);
        ias = GameObject.FindGameObjectsWithTag("IA");
        camAnim = camAnim.gameObject.GetComponent<Animator>();
    }
    private void Update()
    {
        if (Dialogues.current.isTalking)
        {
            if (Input.GetMouseButtonDown(0))
            {
                SwapText();
            }
        }

    }

    void OnTriggerEnter2D(Collider2D other)
    {

        if (other.gameObject.tag == "IA")
        {
            StartDialogue();
        }
    }
    
    

    

    public void StartDialogue()
    {
        Dialogues.current.txtDialogues.text = localSpeech;
        Dialogues.current.isTalking = true;
        Dialogues.current.dialoguesPanel.SetActive(true);
    }

   

    public void SwapText()
    {
        if (Dialogues.current.txtDialogues.text == localSpeech)
        {
            Dialogues.current.txtDialogues.text = localSpeech2;
           
        }
        else if (Dialogues.current.txtDialogues.text == localSpeech2)
        {
            Dialogues.current.txtDialogues.text = localSpeech3;
            
        }
        else if (Dialogues.current.txtDialogues.text == localSpeech3)
        {
            Dialogues.current.txtDialogues.text = localSpeech4;
        }
        else if (Dialogues.current.txtDialogues.text == localSpeech4)
        {
            Dialogues.current.txtDialogues.text = localSpeech5;
        }
        else if (Dialogues.current.txtDialogues.text == localSpeech5)
        {
            Dialogues.current.txtDialogues.text = localSpeech6;
        }
        else if (Dialogues.current.txtDialogues.text == localSpeech6)
        {
            Dialogues.current.txtDialogues.text = localSpeech7;
        }
        else if (Dialogues.current.txtDialogues.text == localSpeech7)
        {
            Dialogues.current.txtDialogues.text = localSpeech8;
        }
        else if (Dialogues.current.txtDialogues.text == localSpeech8)
        {
            Dialogues.current.txtDialogues.text = localSpeech9;
        }
        else if (Dialogues.current.txtDialogues.text == localSpeech9)
        {
            Dialogues.current.txtDialogues.text = localSpeech10;
        }
        else if (Dialogues.current.txtDialogues.text == localSpeech10)
        {
            StopText();
            StartCoroutine(RestartRunning());
        }


    }

    public void StopText()
    {
        Dialogues.current.dialoguesPanel.SetActive(false);
        Dialogues.current.isTalking = false;
    }
    IEnumerator RestartRunning()
    {
        yield return new WaitForSeconds(2f);
        foreach (GameObject ia in ias)
        {
            ia.tag = "Enemy";
        }
        foreach (Animator anim in StopRunning.current.animatorsIA)
        {
            anim.SetBool("Idle", false);
        }
        foreach (IAMoveIntro ia in StopRunning.current.speedIA)
        {
            ia.speed = 4f;
        }
        yield return new WaitForSeconds(1f);
        npc.flipX = true;
        camAnim.SetTrigger("MoveCam");
        yield return new WaitForSeconds(.75f);
        StopRunning.current.animatorNPC.SetTrigger("TrueForm");
        yield return new WaitForSeconds(2f);
        npc.flipX = false;
        StopRunning.current.animatorNPC.SetTrigger("Spell");
        spell.SetActive(true);

        yield return new WaitForSeconds(1f);
        npc.flipX = true;

        StopRunning.current.animatorNPC.SetTrigger("Run");

        NPCRun.current.speed = 3f;


    }
}
